import sys

import requests

from apitest import constants
from apitest.exceptions import HTTPMethodNotSupportedError


def method_dispatcher(*args, **kwargs):
    http_method, url = args

    if http_method not in constants.HTTP_METHOD_NAMES:
        raise HTTPMethodNotSupportedError(http_method)

    handler = getattr(sys.modules[__name__], http_method)

    return handler(*args, **kwargs)


def get(*args, **kwargs):
    url = args[1]
    query_string = kwargs.get('query_string', None)
    headers = kwargs.get('headers', None)

    return requests.get(url, params=query_string, headers=headers)


def post(*args, **kwargs):
    url = args[1]
    query_string = kwargs.get('query_string', None)
    payload = kwargs.get('payload', None)
    headers = kwargs.get('headers', None)

    return requests.post(
        url,
        params=query_string,
        json=payload,
        headers=headers
    )


def put(*args, **kwargs):
    url = args[1]
    query_string = kwargs.get('query_string', None)
    payload = kwargs.get('payload', None)
    headers = kwargs.get('headers', None)

    return requests.put(
        url,
        params=query_string,
        data=payload,
        headers=headers
    )


def delete(*args, **kwargs):
    url = args[1]
    query_string = kwargs.get('query_string', None)
    headers = kwargs.get('headers', None)

    return requests.delete(url, params=query_string, headers=headers)
