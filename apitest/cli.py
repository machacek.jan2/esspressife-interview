import click

from apitest.wrapper import execute_test_scenarios


@click.group()
def main():
    pass


@main.command(short_help='Executes a test scenario from a given .json file.')
@click.argument('file')
def execute(file):
    output = execute_test_scenarios(file)
    click.echo(output)

    return None