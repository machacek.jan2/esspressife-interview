from tabulate import tabulate

from apitest.constants import (
    SLICE_TO_INDEX,
    PRINTER_HEADERS,
    PRINTER_HEADERS_DATA_KEYS
)
from apitest.utils import extract_properties_values_from_json


def _slice_str_args(*args, slice_to=SLICE_TO_INDEX):
    return tuple(
        str_value[:slice_to] for str_value in args
    )


def _format_data_as_tabular(list_data, headers=PRINTER_HEADERS):
    """

    Example table output:

        ╒══════════════════════════╤═════════════════════════════╕
        │ Test name                │   HTTP Response Status Code │
        ╞══════════════════════════╪═════════════════════════════╡
        │ TEST: List all users     │                         200 │
        ├──────────────────────────┼─────────────────────────────┤
        │ TEST: Create an HTML bin │                         200 │
        ╘══════════════════════════╧═════════════════════════════╛

    """
    return tabulate(
        list_data,
        headers,
        tablefmt='fancy_grid',
    )


def prepare_data_for_print(list_of_dicts):
    list_of_strs = []

    for _dict in list_of_dicts:
        args = extract_properties_values_from_json(
            _dict,
            PRINTER_HEADERS_DATA_KEYS
        )
        sliced_str_args = _slice_str_args(*args)
        list_of_strs.append(sliced_str_args)

    return _format_data_as_tabular(list_of_strs)
