from copy import deepcopy

from apitest.http_schemas.base_schema import base_schema

put_schema = deepcopy(base_schema)
put_schema['properties'].update(
    {
        'payload': {
            'type': 'object'
        },
        'query_string': {
            'type': 'object'
        }
    }
)
