from copy import deepcopy

from apitest.http_schemas.base_schema import base_schema

post_schema = deepcopy(base_schema)
post_schema['properties'].update(
    {
        'payload': {
            'type': 'object'
        },
        'query_string': {
            'type': 'object'
        }
    }
)
