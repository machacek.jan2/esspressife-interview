from copy import deepcopy

from apitest.http_schemas.base_schema import base_schema

delete_schema = deepcopy(base_schema)
delete_schema['properties'].update(
    {
        'query_string': {
            'type': 'object'
        }
    }
)
