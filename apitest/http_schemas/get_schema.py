from copy import deepcopy

from apitest.http_schemas.base_schema import base_schema

get_schema = deepcopy(base_schema)
get_schema['properties'].update(
    {
        'query_string': {
            'type': 'object'
        }
    }
)
