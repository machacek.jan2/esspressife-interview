from apitest import parser


def execute_single_test_scenario(json_data):

    required_args, optional_kwargs = parser.extract_json_data(json_data)
    http_method, url = parser.prepare_request_args(*required_args)
    response = parser.send_http_request(http_method, url, **optional_kwargs)
    response_content = parser.extract_http_response_content(response)
    response_content['name'] = json_data['name']
    response_content['cover'] = ' % - COMING SOON'

    return response_content


def execute_multiple_test_scenarios(list_of_dicts):
    results = [
        execute_single_test_scenario(json_data) for json_data in list_of_dicts
    ]

    return results


def execute_test_scenarios(file):
    try:
        list_of_content = core.load_content_from_json_file(file)
        # Actually, the zeroth index contains the content
        content = list_of_content[0]

        # Depends on a type of the content loaded from the file,
        # the business logic for single or multiple test scenarios
        # is executed.
        if isinstance(content, dict):
            result = execute_single_test_scenario(content)
        elif isinstance(content, list):
            result = execute_multiple_test_scenarios(content)
    except Exception as exc:
        return str(exc)

    return parser.transform_data_in_tabular_str(result)