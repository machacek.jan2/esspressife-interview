import ijson.backends.yajl2_c as ijson

from requests import Response

from apitest import utils
from apitest import constants
from apitest.http import method_dispatcher
from apitest.printer import prepare_data_for_print
from apitest.decorators import (
    check_file_extension,
    validate_data_against_json_schema
)


@check_file_extension
def load_content_from_json_file(file_path):
    with open(file_path, 'rb') as file:
        items_generator = ijson.items(file, '')
        list_of_items = [item for item in items_generator]

        return list_of_items


@validate_data_against_json_schema
def extract_json_data(data):
    required_args = utils.extract_properties_values_from_json(
        data,
        constants.REQUIRED_SCHEMA_KEYS
    )
    optional_kwargs = utils.extract_properties_values_of_type_dict_from_json(
        data,
        constants.OPTIONAL_SCHEMA_KEYS
    )

    return (required_args, optional_kwargs)


def prepare_request_args(*args):
    if not args or len(args) != 4:
        return None

    _, http_method, endpoint, host = args
    url = utils.prepare_url(host, endpoint)

    return (http_method.lower(), url)


def send_http_request(*args, **kwargs):
    return method_dispatcher(*args, **kwargs)


def extract_http_response_content(response):
    if not isinstance(response, Response):
        return None

    return {
        'status_code': str(response.status_code),
    }


def transform_data_in_tabular_str(data):
    if not any(isinstance(data, _type) for _type in [list, dict]):
        return 'The data is not correctly structured.'

    if isinstance(data, list) and not isinstance(data[0], dict):
        return 'The list of content is not correctly formatted.'

    if isinstance(data, dict):
        # Put the `dict` data into a list
        data = [data]

    return prepare_data_for_print(data)