from click.testing import CliRunner

from apitest import cli


def test_execute():
    runner = CliRunner()
    result = runner.invoke(cli.execute, ['test.json'])

    assert result.exit_code == 0
