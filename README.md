# Esspressife interview

Installation
******************************************

Recommended installation method is to use ``pip``:


    $ pip install -r requirements.txt

Python version **3+** is required.


Usage
******************************************

    $ apitest/cli.py execute FILE_PATH/Test1.json

See also ``apitest/cli.py --help``.
------------------------------------------


``FILE: Test1.json``

```

[
  {
    "name":"TEST: Task1",
    "verb":"GET",
    "endpoint":"api/v1/task1",
    "host":"http://localhost:8080/",
    "headers":{
       "Accept-Language":"en-US"
    },
    "query_string":{
       "State":1
    }
  },
  {
    "name":"TEST: Task2",
    "verb":"POST",
    "endpoint":"api/v1/login",
    "host":"http://localhost:8080/",
    "payload":{
       "username":"jan",
       "pass":"12345"
    }
  }
]
```