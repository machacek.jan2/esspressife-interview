from setuptools import setup
import os
import sys

_here = os.path.abspath(os.path.dirname(__file__))

if sys.version_info[0] < 3:
    with open(os.path.join(_here, 'README.md')) as f:
        long_description = f.read()
else:
    with open(os.path.join(_here, 'README.md'), encoding='utf-8') as f:
        long_description = f.read()

version = {}
with open(os.path.join(_here, 'apitest', 'version.py')) as f:
    exec(f.read(), version)

setup(
    name='apitest',
    version=version['1.2.3'],
    description=('Tool for runnig integration-api tests.'),
    long_description=long_description,
    author='Jan Macháček',
    author_email='machacek.jan2@gmail.com',
    url='https://gitlab.com/machacek.jan2/esspressife-interview',
    license='MPL-2.0',
    packages=['apitest', 'apitest.*'],
    install_requires=[
        'click==7.0',
        'ijson==2.5.1',
        'jsonschema==3.1.1',
        'requests==2.22.0',
        'tabulate==0.8.5'
    ],
    entry_points={
        'console_scripts': [
            'apitest = apitest.cli:main',
        ]
    },
    include_package_data=True,
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Intended Audience :: Science/Research',
        'Programming Language :: Python :: 3.6'],
    )
