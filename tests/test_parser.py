from unittest.mock import patch

import pytest

from requests import Response

from apitest import parser
from apitest.exceptions import FileExtensionError


def test_load_content_from_json_file():
    content = parser.load_content_from_json_file('data/HTTP_GET.json')

    assert isinstance(content[0], dict)


def test_load_content_from_json_file_with_not_supported_file_extension():
    with pytest.raises(FileExtensionError) as exc:
        parser.load_content_from_json_file('data/HTTP_GET.yaml')

    assert 'is not supported' in str(exc.value)


def test_extract_json_data():
    content = parser.load_content_from_json_file('data/HTTP_GET.json')
    required_args, optional_kwargs = parser.extract_json_data(content[0])

    assert isinstance(required_args, tuple) and isinstance(optional_kwargs, dict)


def test_extract_json_data_with_key_name_response():
    content = parser.load_content_from_json_file('data/HTTP_GET_SAVE_RESPONSE.json')
    required_args, optional_kwargs = parser.extract_json_data(content[0])

    assert 'response' in optional_kwargs and optional_kwargs['response']


def test_prepare_request_args():
    args = (
        'TEST: List all users',
        'GET',
        'users',
        'http://localhost:8080'
    )
    request_args = parser.prepare_request_args(*args)
    expected_args = ('get', 'http://localhost:8080/users')

    assert sorted(request_args) == sorted(expected_args)


def test_prepare_request_args_with_invalid_arguments():
    args = (
        'users',
        'http://localhost:8080'
    )
    request_args = parser.prepare_request_args(*args)

    assert request_args is None


@patch('apitest.parser.method_dispatcher', return_value=Response)
def test_send_http_request(mock):
    mock.return_value.status_code = 200
    args = ('get', 'http://localhost:8080/users')
    response = parser.send_http_request(*args)

    assert response.status_code == 200


def test_extract_http_response_content():
    response = Response()
    response.status_code = 200
    response.headers = {'Content-Type': 'application/json'}
    response_content = parser.extract_http_response_content(response)

    assert all(
        key in response_content for key in ('status_code',)
    )


def test_extract_http_response_content_with_not_supported_argument_type():
    response = {
        'status_code': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': '<h1>Hello, apitest!</h1>'
    }
    response_content = parser.extract_http_response_content(response)

    assert response_content is None


def test_load_content_from_json_file_with_multiple_http_requests_scenarios():
    content = parser.load_content_from_json_file(
        'data/MULTIPLE_HTTP_REQUESTS.json'
    )

    assert isinstance(content[0], list)


def test_transform_data_in_tabular_str_with_dict_data():
    data = {
        'name': 'Test: process data for print',
        'status_code': '200',
        'headers': '{Content-Type: application/json}',
        'body': 'Lorem Ipsum'
    }
    tabular_str = parser.transform_data_in_tabular_str(data)

    assert isinstance(tabular_str, str)


def test_transform_data_in_tabular_str_with_list_of_dict_data():
    data = [{
        'name': 'Test: process data for print',
        'status_code': '200',
        'headers': '{Content-Type: application/json}',
        'body': 'Lorem Ipsum'
    }]
    tabular_str = parser.transform_data_in_tabular_str(data)

    assert isinstance(tabular_str, str)
